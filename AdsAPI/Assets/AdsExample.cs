﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using UnityEngine.UI;
using TMPro;

public class AdsExample : MonoBehaviour
{

    public TextMeshProUGUI interadText, rewardedAdtext,testtext;
   

    void Awake()
    {
        testtext.text = "READY";

        if (!RuntimeManager.IsInitialized())
        {
            RuntimeManager.Init();
        }
            
    }
    private void Update()
    {

        if (Advertising.IsInterstitialAdReady())
        {
            interadText.text = "This Ad is Ready";
            interadText.color = Color.green;
        }
        else
        {
            interadText.text = "This Ad is Not Ready";
            interadText.color = Color.red;
        }

        if (Advertising.IsRewardedAdReady())
        {
            rewardedAdtext.text = "This Ad is Ready";
            rewardedAdtext.color = Color.green;
        }
        else
        {
            rewardedAdtext.text = "This Ad is Not Ready";
            rewardedAdtext.color = Color.red;
        }
      
    }
    void RewardedAdCompletedHandler(RewardedAdNetwork network, AdPlacement location)
    {
        testtext.text = "Toma tu Premio!";
    }

    public void ShowBannerAd()
    {
        Advertising.ShowBannerAd(BannerAdPosition.Top);
    }

    public void ShowInterstitialAd()
    {
        if (Advertising.IsInterstitialAdReady())
        {
            Advertising.ShowInterstitialAd();
        }
    }
    public void ShowRewardedAd()
    {
        if (Advertising.IsRewardedAdReady())
        {
            Advertising.ShowRewardedAd();
            Advertising.RewardedAdCompleted += RewardedAdCompletedHandler;
        }
    }

}
